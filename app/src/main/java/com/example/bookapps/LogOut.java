package com.example.bookapps;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LogOut extends AppCompatActivity{

    FirebaseAuth.AuthStateListener authListener;
    FirebaseAuth auth;
    Button Logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_user);

        Logout = findViewById(R.id.logout);
        auth = FirebaseAuth.getInstance();

//        FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//
//                Logout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        //Fungsi untuk logout
//                        if (v  ==Logout){
//                            FirebaseAuth.getInstance().signOut();
//                            finish();
//                            startActivity(new Intent(LogOut.this, LoginActivity.class));
//                            Toast.makeText(LogOut.this,"SignOut Succes",Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//            }
//        };

    }

    public void Moveout(View view) {
        if (view ==Logout){
            FirebaseAuth.getInstance().signOut();
            finish();
            startActivity(new Intent(this,LoginActivity.class));
            Toast.makeText(LogOut.this,"SignOut Succes",Toast.LENGTH_SHORT).show();
        }
    }

}

