package com.example.bookapps;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    private NavigationBarView.OnItemSelectedListener navigation = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            Fragment f = null;
            switch (item.getItemId()){
                case R.id.home_menu:
                    f = new FragmentHome();
                    break;
                case R.id.book_menu:
                    f = new FragmentBook();
                    break;
                case R.id.payment_menu:
                    f = new FragmentPayment();
                    break;
                case R.id.inbox_menu:
                    f = new FragmentInbox();
                    break;
                case R.id.user_menu:
                    f = new FragmentUser();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container_fragment,f).commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.buttom_navigation_menu);
        bottomNavigationView.setOnItemSelectedListener(navigation);
    }
}